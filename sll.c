#include<stdio.h>
#include<stdlib.h>

struct node{
	int data;
	struct node * next;
};

struct node * insert_at_end(struct node  * head,int x){
	struct node * temp=(struct node *)malloc(sizeof(struct node));
	if(head==NULL)
	{
		temp->data=x;
		temp->next=NULL;
		head=temp;
		return head;

	}
	else{
		temp->data=x;
		temp->next=head;
		head=temp;
		return head;
	}
}

struct node * insert_at_loc(struct node * head, int x,int pos){
	struct node * templhs, *temprhs, *new_node;
	int countr,total_count;
	if(head==NULL && pos < 1){
		printf("Position invalid\n");
		return head;
	}
	else if(head==NULL && pos==1){
		head->data=x;
		head->next=NULL;
		return head;
	}
	else if(pos > (count_elements(head)+1)){
		printf("Position invalid\n");
		return head;
	}
	else if(pos==(count_elements(head)+1)){
		templhs=(struct node *)malloc(sizeof(struct node));
		templhs->data=x;
		templhs->next=head;
		head=templhs;
		return head;
	}
	else{
		total_count=count_elements(head);
		temprhs=head;
		templhs=NULL;
		while((total_count-countr)!=pos && temprhs->next!=NULL)
		{
			templhs=temprhs;
			temprhs->next;
			countr++;
		}
		if(temprhs->next==NULL){
			new_node=(struct node *)malloc(sizeof(struct node));
			new_node->data=x;
			new_node->next=NULL;
			temprhs->next=new_node;
		}
		else{
			new_node=(struct node *)malloc(sizeof(struct node));
			new_node->data=x;
			new_node->next=temprhs;
			templhs->next=new_node;
		}
		return head;


	}

}
void display_elements(struct node * head){
	if(head==NULL)
	{
		printf("No element in the list\n");
		return;
	}
	else{
		struct node * temp;
		temp=head;
		printf("Elements are: \n");
		while(temp!=NULL)
		{
			printf("\t %d",temp->data);
			temp=temp->next;
		}
		free(temp);
	}
}
int count_elements(struct node * head){
	int cnt=0;
	if(head==NULL)
	{
		printf("No element in the list\n");
		return 0;
	}
	else{
		struct node * temp;
		temp=head;
		while(temp!=NULL)
		{
			cnt++;
			temp=temp->next;
		}
		return cnt;
	}

}
struct node * delete_at_end(struct node *  head){
	if(head==NULL){
		printf("List is empty\n");
		return NULL;
	}
	else if(head->next==NULL){
		printf("Deleted element is : \t %d",head->data);
		head=NULL;
		return head;
	}
	else{
		struct node * temp;
		temp=head;
		printf("Deleted element is : \t %d",temp->data);
		head=head->next;
		free(temp);
		return head;
	}

}
struct node * delete_at_loc(struct node * head,int pos){
	struct node * temp,*templhs,*temprhs;
	int countr,total_count;
	if(head==NULL){
		printf("No element in the list\n");
		return head;
	}
	else if(pos > count_elements(head)){
		printf("Position invalid\n");
		return head;
	}
	else if(pos==count_elements(head)){
		temp=head;
		head=head->next;
		printf("\nDeleted element is : %d",temp->data);
		free(temp);
		return head;
	}
	else{
		total_count=count_elements(head);
		temprhs=head;
		templhs=NULL;
		while((total_count-countr)!=pos && temprhs->next !=NULL)
		{
			templhs=temprhs;
			temprhs->next;
			countr++;
		}
		if(temprhs->next==NULL){
			printf("\nDeleted element is : %d",temprhs->data);
			temprhs=NULL;
		}
		else{
			printf("\nDeleted element is : %d",temprhs->data);
			templhs->next=temprhs->next;
			free(temprhs);
		}
		return head;
	}

}


int main()
{
	int i,num,loc,count;
	struct node * head;
	head=NULL;

	printf("=========================\n");
	printf("Operations: \n");
	printf("1. Insert at end \n");
	printf("2. Insert at a location \n");
	printf("3. Display all elements \n");
	printf("4. Count the number of elements \n");
	printf("5. Delete at end \n");
	printf("6. Delete at a given location\n");
	printf("=========================\n");

	printf("\nEnter an integer for above choices\n ");
	scanf("%d", &i);

    while(1){
		switch(i){
			case 1 :  printf("\nYou chose :  Insertion at end.");
					  printf("\nEnter element: \t");
					  scanf("%d",&num);
					  head=insert_at_end(head,num);
					  break;
			case 2 :  printf("\nYou chose :  Insertion at a location.");
					  printf("\nEnter element: \t");
					  scanf("%d",&num);
					  printf("\nEnter location: \t");
					  scanf("%d",&loc);
					  head=insert_at_loc(head,num,loc);
					  break;
			case 3 :  printf("\nYou chose :  Display all elements.");
					  display_elements(head);
					  break;
			case 4 :  printf("\nYou chose :  Count the number of elements.");
					  count= count_elements(head);
					  printf("\nNo. of elements is :  %d",count);
					  break;
			case 5 :  printf("\nYou chose :  Deletion at end.");
					  head=delete_at_end(head);
					  break;
			case 6 :  printf("\nYou chose :  Deletion at location.");
					  printf("\nEnter location to delete from: \t");
					  scanf("%d",&loc);
					  head=delete_at_loc(head,loc);

					  break;
			default : exit(0);
		}
		printf("\nEnter an integer for above choices\n ");
        scanf("%d", &i);
    }
    return 0;

}

#include<stdio.h>

/* Find the number of BST's possbile with a given number of keys */



int count_trees(int n){

	if(n<=1)
		return 1;
	else{
        int sum=0;
		int left,right,root;
		for(root=1;root<=n;root++){
			left=count_trees(root-1);
			right=count_trees(n-root);

			sum+=left*right;
		}
		return sum;
	}

}


int main()
{
	int n,no_of_trees;
	printf("Enter the value of n \n");
	scanf("%d",&n);
	no_of_trees=count_trees(n);
	printf("No. Of BSTs possible are : %d\n",no_of_trees);
	return 0;
}


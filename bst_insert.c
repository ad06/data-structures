#include<stdio.h>
#include<stdlib.h>

struct treenode{
	int data;
	struct treenode * left;
	struct treenode * right;
};

struct treenode * insert_element(struct treenode * root,int a)
{
	if(root==NULL)
	{
		root= (struct treenode *)malloc(sizeof(struct treenode));
		root->left=NULL;
		root->right=NULL;
		root->data=a;

	}
	else{

		if(a < root->data){
			root->left=insert_element(root->left,a);
		}
		else{
			root->right=insert_element(root->right,a);
		}

	}
	return root;
}

void display_elements_inorder(struct treenode * root){

	if(root==NULL){
		return;
	}
	else{

			display_elements_inorder(root->left);
			printf("%d\t",root->data);
			display_elements_inorder(root->right);
	}
}


int main()
{

	int i,num,loc,count;
	struct treenode * root;
	root=NULL;

	printf("=========================\n");
	printf("Operations: \n");
	printf("1. Insert an element \n");
	printf("2. Display inorder traversal \n");
	/*printf("3. Display all elements \n");
	printf("4. Count the number of elements \n");
	printf("5. Delete at end \n");
	printf("6. Delete at a given location\n");*/
	printf("=========================\n");

	printf("\nEnter an integer for above choices\n ");
	scanf("%d", &i);

    while(1){
		switch(i){
			case 1 :  printf("\nYou chose :  Insertion.");
					  printf("\nEnter element: \t");
					  scanf("%d",&num);
					  root=insert_element(root,num);
					  break;
			case 2 :  printf("\nYou chose :  Display all elements.");
					  display_elements_inorder(root);
					  break;
			/*case 3 :  printf("\nYou chose :  Display all elements.");
					  display_elements(head);
					  break;
			case 4 :  printf("\nYou chose :  Count the number of elements.");
					  count= count_elements(head);
					  printf("\nNo. of elements is :  %d",count);
					  break;
			case 5 :  printf("\nYou chose :  Deletion at end.");
					  head=delete_at_end(head);
					  break;
			case 6 :  printf("\nYou chose :  Deletion at location.");
					  printf("\nEnter location to delete from: \t");
					  scanf("%d",&loc);

					  head=delete_at_loc(head,loc);
					  break;*/
			default : exit(0);
		}
		printf("\nEnter an integer for above choices\n ");
        scanf("%d", &i);
    }
    return 0;


}


